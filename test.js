
var http = require('http')

var options = {
  host: 'localhost',
  port: 8080,
  path: '/facebook/users'
};

http.get(options, function(res) {
    var content = '';
    res.on("data", function (chunk) {
        content += chunk;
    });
    res.on("end", function () {
        console.log(content);
    });
}).on('error', function(e) {
  console.log("Got error: " + e.message);
});

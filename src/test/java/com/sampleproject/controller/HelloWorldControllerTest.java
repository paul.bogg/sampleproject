package com.sampleproject.controller;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.ui.ModelMap;

public class HelloWorldControllerTest {

    @Test
    public void shouldReturnTestPageWhenCallingMethod() {
        HelloWorldController helloWorldController = new HelloWorldController();

        Assert.assertThat(helloWorldController.getTest2(), CoreMatchers.notNullValue());
        Assert.assertThat(helloWorldController.getTest2(), CoreMatchers.not("testpage3"));
        Assert.assertThat(helloWorldController.getTest2(), CoreMatchers.equalTo("testpage2"));
    }


    // Unit tests
    // Every unit test starts with @Test - this is an annotation from JUnit (org.junit.Test)
    @Test
    public void shouldSetTitleNameOnGetRequest() {
        HelloWorldController helloWorldController = new HelloWorldController();

        // 'mock' is a method that creates an object that 'impersonates' a class, but really, is a fake class
        // that you can run checks against
        ModelMap modelMapMock = Mockito.mock(ModelMap.class);
        helloWorldController.get(modelMapMock);

        // 'verify' is a method that checks whether a method on your 'mock' object was called
        Mockito.verify(modelMapMock, Mockito.times(1)).addAttribute(Matchers.anyString(), Matchers.anyString());
        Mockito.verify(modelMapMock, Mockito.times(1)).addAttribute("title", "Title name");
    }

    // TODO write some more @Test methods
    // Throwing you into the deep end, see if you can write another test (or more) for the post() method

}

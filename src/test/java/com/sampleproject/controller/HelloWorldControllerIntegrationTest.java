package com.sampleproject.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class HelloWorldControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    // Don't worry about this, I will explain 'integration' tests later
    @Test
    public void shouldNotAllowNullNameOnPost() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/").param("name", ""))
            .andExpect(MockMvcResultMatchers.status().is(400));
    }

}

package com.sampleproject.controller;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserEntity {

    @NotNull
    @Size(min = 10)
    private String name;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

package com.sampleproject.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ErrorControllerHandler {

    @ExceptionHandler(Exception.class)
    public void handlGeneralExceptions(Exception error) {
        System.out.println(error);
    }

}

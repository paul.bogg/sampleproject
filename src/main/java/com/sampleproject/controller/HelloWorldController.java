package com.sampleproject.controller;

import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

// Controllers expose web interfaces - I will explain these

@RestController
public class HelloWorldController {

    @GetMapping(path = {"/test"})
    public String getTest() {
        return "Hello world";
    }

    @GetMapping(path = {"/facebook/users"})
    public UserEntity getTest2() {
        UserEntity user = new UserEntity();
        user.setAddress("Some address");
        user.setName("Person");
        return user;
    }

    @GetMapping(path = {"/", ""})
    public String get(ModelMap modelMap) {
        modelMap.addAttribute("title", "Title name");
        return "index";
    }

    @PostMapping(path = {"/", ""})
    public String post(@Valid UserEntity userEntity, BindingResult result, RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", String.format("'%s' is too short", result.getFieldError().getRejectedValue()));
        }
        redirectAttributes.addFlashAttribute("result", userEntity.getName());
        return "redirect:/";
    }

}
